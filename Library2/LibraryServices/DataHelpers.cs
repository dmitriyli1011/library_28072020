﻿using LibraryData.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryServices
{
    public class DataHelpers
    {
        public static IEnumerable<string> HummanizeBizHours(IEnumerable<BranchHours> branchHours)
        {
            var hours = new List<string>();

            foreach (var time in branchHours)
            {
                var day = HummanizeDay(time.DayOfWeek);
                var openTime = HummanizeTime(time.OpenTime);
                var closeTime = HummanizeTime(time.CloseTime);

                var timeEntry = $"{day} {openTime}to{closeTime}";
                hours.Add(timeEntry);
            }
            return hours;
        }

        public static string HummanizeDay(int number)
        {
            return Enum.GetName(typeof(DayOfWeek), number-1);
        }

        public static string HummanizeTime(int time)
        {
            return TimeSpan.FromHours(time).ToString("hh':'mm");
        }
    }
}
